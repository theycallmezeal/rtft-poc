import yaml
from jinja2 import Template
import pathlib

pathlib.Path('./public').mkdir(parents=True, exist_ok=True)

with open('template.html') as template:
    template = Template(template.read())

with open("rtft-config.yml", "r") as yaml_file:
    config = yaml.safe_load(yaml_file)

output = template.render(variable=config['variable'])

output_file = open("public/index.html", "w+")
output_file.write(output)
output_file.close()
